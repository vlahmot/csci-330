Results:

The order from fastest to slowest in array declaration times is static arrays, arrays on the stack, dynamic arrays. This is as you would expect. The static array declaration is quick because it's memory requirement is known at compile time, and its memory allocation takes place during load time. The static modifier causes the variable to be bound to memory cells before execution time.

The stack dynamic arrays are the middle runners. With the stack dynamic array storage binding happens when the declaration statement is elaborated. The stack dynamic array has overhead of allocation and deallocation as well as indirect addressing.

The heap dynamic array is the slowest of all 3 array types. These arrays are the slowest because their memory requirements are not known at compile time. Instead the size requirement is specified during runtime. The process of declaring and allocating a heap dynamic array is inefficient due to the dynamic size of the array and because it lies on the heap instead of on the stack. 


