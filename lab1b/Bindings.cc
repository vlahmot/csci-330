#include<cstdio>
#include <sys/time.h>
void
stackArray()
{
  int A[100000];
}

void 
staticArray()
{
  static int A[100000];
}

void 
dynamicArray()
{
  int* A = new int[100000];
  delete[] A;
}

double
timeFunctionCalls( void (*f)(), int numCalls)
{
  struct timeval startTime;
  struct timeval endTime;
  gettimeofday(&startTime, NULL);
  for (size_t i = 0; i < numCalls; ++i)
  {
    f();
  }
  gettimeofday(&endTime, NULL);
  double tS = startTime.tv_sec * 1000000 + (startTime.tv_usec);
  double tE = endTime.tv_sec * 1000000 + (endTime.tv_usec);

  return (tE - tS);
}



int
main()
{
  int numCalls = 10000000;
  printf("Time for declaring %d arrays on the stack is: %f\n",numCalls,timeFunctionCalls(stackArray,numCalls));
  printf("Time for declaring %d static arrays is: %f\n",numCalls, timeFunctionCalls(staticArray,numCalls));
  printf("Time for declaring %d dynamic arrays is: %f\n",numCalls, timeFunctionCalls(dynamicArray,numCalls));
}
