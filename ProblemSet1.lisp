;Author: Tom Hlavaty
;Date: 2/7
;Problem Set 1

(defun mygcd (a b)
	(cond
		((= b 0) a)
		(t(mygcd b (mod a b)))
	)
)

(defun flatten (ls)
	
	(cond 
                ((null ls) nil)
		((atom (first ls) ) (cons (first ls) (flatten (rest ls)))    ) 
		((listp (first ls))   (append (flatten(first ls)) (flatten(rest ls))))
	)
)

(defun delete_elem (a e)  
  (cond    
    (
      (equal nil a) nil
    )

    ( 
      (not (equal (first a) e )) 
      (cons (first a) (delete_elem (rest a) e))
    )
   
    (
      (equal (first a) e ) 
      (delete_elem (rest a) e)    
    )

  )                                    
)

(defun del_if (ls pred) 
	(cond
		(
			(equal nil ls) 
    	(RETURN-FROM del_if)
  	)

  	(
    	(not(funcall pred (first ls) ))
    	(cons (first ls) (del_if  (rest ls) pred))
  	)

  	(
			(funcall pred (first ls))
    	(del_if (rest ls) pred)
 		)
	)
)

(defun pow (x n) (cond ((= n 1)x ) (t(* x (pow x (- n 1)))) )  )
(defun myexp (n)  #'(lambda (x) (pow n x)) )
  

