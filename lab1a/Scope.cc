/* A function for exploring the scoping in C++.
 */
#include<cstdio>
int x;
void
scope ()
{
  printf("Global X is declared.\n");

  x = 21;
  printf("x is set to 21\n");

  printf("Printing X produces: %d\n",x);

  int x;
  printf("Local X is declared.\n");

  printf("Printing X produces: %d\n",x);

  x = 42;
  printf("x is set to 42\n");

  printf("Printing X produces: %d\n",x);
  printf("Printing the Global X produces: %d\n", ::x);
}

int
main ()
{
  scope();
}
