/* A function for exploring the scoping rules of C.
 */
#include <stdio.h>
int x;

void scope (void)
{
  printf("Global X is declared.\n");

  x = 21;
  printf("x is set to 21\n");

  printf("Printing X produces: %d\n",x);

  int x;
  printf("Local X is declared.\n");

  printf("Printing X produces: %d\n",x);


  x = 42;
  printf("x is set to 42\n");

  printf("Printing X produces: %d\n",x);

  {
    extern int x;
    printf("Printing the Global X produces: %d\n",x);
  }
}

int 
main(void)  
{
 scope(); 
}
