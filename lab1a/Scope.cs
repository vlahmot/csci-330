/* A function for exploring the scoping rules in C#. Does not compile.
 * Declaring the local variable x in Scope hides the class variable.
 * This causes the use of x in the statement "x = 21;" invalid as 
 * x has not yet been declared in the scope.
 *
 *
 */

class ScopeTest
{
  static int x;

  public static void Scope()
  {
    System.Console.WriteLine("Global X declared.");
    x = 21;
    System.Console.WriteLine("X is set to 21");
    System.Console.WriteLine("Printing X produces {0}", x);
    int x;
    System.Console.WriteLine("Local X is declared.");
    x = 42;
    System.Console.WriteLine("X is set to 42.");
    System.Console.WriteLine("Printing X produces {0}", x);
  }

  public static void Main()
  {
    Scope();
  }
}
