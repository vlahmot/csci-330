from Board import Board
from HumanPlayer import HumanPlayer

class Game:
  
  def __init__(self):
    self.gameBoard = Board()
    self.player1 = HumanPlayer('X')
    self.player2 = HumanPlayer('O')
    self.turnCount = 0 

  def playGame(self):
    """A method for playing Connect Four."""     
    while self.getGameState() == None:
      self.takeTurn(self.player1)
      if self.getGameState() == None:
        self.takeTurn(self.player2)  
    
    self.turnCount = str(self.turnCount) #change type of variable to string

    gameResult = self.getGameState()
    if gameResult[0] == 1 or gameResult[0] == 2:
      self.gameBoard.highlightWinningRun(gameResult)
      self.gameBoard.printBoard()
      gameResult = str(gameResult[0])
      
      print 'Player ' + gameResult + ' has won in ' + self.turnCount + ' turns.'
    if gameResult[0] == 3:
      print 'The Game was a Tie in ' + self.turnCount + ' turns.'
      self.gameBoard.printBoard()


  def takeTurn(self,player):
    """A method for taking a Connect Four turn. Checks for full
       columns before placing a marker."""
    self.gameBoard.printBoard()
    currMove = player.move()
    while self.gameBoard.checkCol(int(currMove)-1) == True:
      currMove = player.move()
    self.gameBoard.placeChip(int(currMove),player.marker)
    self.turnCount +=1
      
  def getGameState(self):
    """A method for determining the state of the game"""
    return self.gameBoard.checkStatus() # this can be None type, or a list.
