from Player import Player

class HumanPlayer(Player):
  """A class for representing a human Connect Four player."""
  def move (self):
    """ A method for making a ConnectFour move."""
    if (self.marker == 'X'):
      player = 1
    else:
      player = 2
    input =raw_input('Player ' + str(player) + " , ("  + self.marker + "), what is your move?")
    while(input.isdigit() == False or input.isdigit() == True and int(input) < 1 or int(input) > 7):
       input = raw_input('Please enter a valid column number [1-7].')
    return input

  def __init__(self,p_marker):
    self.marker = p_marker
