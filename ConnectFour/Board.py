import sys

class Board:
  """A class for representing a ConnectFour Board."""
  def __init__(self):
    self.emptyToken = '-'
    self.playerow1Token = 'X'
    self.playerow2Token = 'O'
    self.rows = [[self.emptyToken] * 7 for i in range(6)]

  def getRow(self, row):
    """ Gets a row of the Board."""
    return self.rows[row]

  def getCol(self,col):
    """ Gets a column of the Board. """
    colVals = []
    for row in self.rows:
      colVals += row[col]
    return colVals

  def getDiaganol(self,row, col, direction):
    """ A method for retrieving diaganol runs of length 4 from the board.
        (row,col) represents the begining of the run, and the direction
        indicated the orientation of the run."""

    diagVals = []

    if direction == 'right':
      if row >= 3  and col <= 3:
        for x in range(0,4):
          diagVals += self.rows[row][col]
          row -= 1
          col += 1

    if direction == 'left':
      if row >= 3 and col >= 3:
        for x in range(0,4):
          diagVals += self.rows[row][col]
          row -= 1
          col -= 1

    return diagVals 

  def checkTie(self):
    """ A method for checking the current Board for a tie game. """
    count = 0
    for x in range(0,7):
      if self.checkCol(x) == True:
        count += 1
    if count == 7:
      return True

  def checkDiagForWin(self):
    """ A method for checking all possible winning diaganol runs. """
    for y in range(0,7):
      for x in range(0,6):
        diagRight = self.getDiaganol(x, y, 'right')
        diagLeft = self.getDiaganol(x, y, 'left')
      
        if diagRight != []:
          playerow1Count = 0
          playerow2Count = 0
          
          for item in diagRight:
            if item == self.playerow1Token:
              playerow1Count += 1
            if item == self.playerow2Token:
              playerow2Count += 1

          if playerow1Count == 4:
            return [1, x, y, x - 3, y + 3]
          if playerow2Count == 4:
            return [2, x, y, x - 3, y + 3]

        if diagLeft != []:
          playerow1Count = 0
          playerow2Count = 0
          
          for item in diagLeft:
            if item == self.playerow1Token:
              playerow1Count += 1
            if item == self.playerow2Token:
              playerow2Count += 1

          if playerow1Count == 4:
            return [1, x, y, x - 3, y - 3]
          
          if playerow2Count == 4:
            return [2, x, y, x - 3, y - 3]
        
  def checkColsForWin(self):
    """ A method for checking all possible Column runs for a winning Board. """
    cols = []
    for x in range(0,7):
      cols.append(self.getCol(x))
    for i in range(0,7):
      for j in range(0,3):
        playerow1Count = 0
        playerow2Count = 0
        
        for k in range(0,4):
          if cols[i][j + k] == self.playerow1Token:
            playerow1Count += 1 
          if cols[i][j + k] == self.playerow2Token:
            playerow2Count += 1
        
        if playerow1Count == 4:
          return [1, j, i, j +3, i]
        
        if playerow2Count == 4:
          return [2, j, i, j+3, i]

  def checkRowsForWin(self):
    """ A method for checking all possible Row runs for a winning Board."""
    for i in range(0,6):
      for x in range(0,4):
        playerow1Count = 0
        playerow2Count = 0

        for y in range(0,4):
          if self.rows[i][x + y] == self.playerow1Token:
            playerow1Count += 1
          if self.rows[i][x + y] == self.playerow2Token:
            playerow2Count += 1

        if playerow1Count == 4:
          return [1, i, x, i, x+3] 
        if playerow2Count == 4: 
          return [2, i, x, i, x+3] 
        
  def checkStatus(self):
    """ A method for determining the status of the Board. 
        Checks for wins and ties."""
    if self.checkTie() == True:
      return [3, 0, 0]
    currentRowStatus = []

    currentRowStatus = self.checkRowsForWin()
    if currentRowStatus != None:
      if currentRowStatus[0] == 1:
        return currentRowStatus
      if currentRowStatus[0] == 2:
        return currentRowStatus

    currentColStatus = self.checkColsForWin()
    if currentColStatus != None:
      if currentColStatus[0] == 1 or currentColStatus[0] == 2:
        return currentColStatus
    
    currentDiagStatus = self.checkDiagForWin()
    if currentDiagStatus != None:
      if currentDiagStatus[0] == 1 or currentDiagStatus[0] == 2:
        return currentDiagStatus

  def placeChip(self, col, player):
    """ A method for placing chips in a column. """
    col-=1
    placeToInsert = -1
    for x in range(0, 6):
      if self.rows[x][col] != self.emptyToken:
        placeToInsert = x - 1
        break
    self.rows[placeToInsert][col] = player
    return True
  
  def checkCol(self,col):
    """Checks if a column is full."""
    if self.rows[0][int(col)] != self.emptyToken:
      return True
    return False

  def getLine(self,coords):
    """ A method for getting the locations of a winning run on a Board."""
    row1 = coords[1]
    col1 = coords[2]
    row2 = coords[3]
    col2 = coords[4]

    if row1 == row2:
      return [[row1, col1], [row1, col1+1], 
      [row1, col1+2], [row2, col2]]
    
    if col1 == col2:
      return [[row1, col1], [row1+1, col1],
      [row1+2, col2], [row2, col2]]

    else:
      if col2 > col1:
        return [[row1, col1], [row1 - 1, col1 + 1],
        [row1 - 2, col1 + 2], [row2, col2]]
      else:
        return [[row1, col1], [row1 - 1, col1 - 1],
        [row1  -2, col1 - 2], [row2, col2]]

  def highlightWinningRun(self,coords):
    """A method for changing the tokens of a winning run."""
    winningCoords = self.getLine(coords)
    for item in winningCoords:
      self.rows[item[0]][item[1]] = '*'

    
  def printBoard(self):
    """ A method for printing the playing board."""
    self.printRows()
    self.printFooter()
  
  def printFooter(self):
    """ A method for printing the display footer."""
    print "+---------------------+"
    print "  1  2  3  4  5  6  7  "
    
  def printRows(self):
    """ A method for printing each row of the play field.
        Each row is bounded by | characters on each side and 
        deliminated by spaces."""
    for row in self.rows:
      sys.stdout.write("|")
      for item in row:
        print "",
        print item,
      print "|"
