(defun del_if (pred ls) 

(cond
  (
    (equal nil ls) 
    (RETURN-FROM del_if)
  )

  (
    (not(funcall pred (first ls) ))
    (cons (first ls) (del_if pred (rest ls)))
  )

  (
    (funcall pred (first ls))
    (del_if pred (rest ls))
  )




)


)
