;Author: Tom Hlavaty
;Date: 2/7
;Problem Set 1

(defun mygcd (a b)
(cond
((= b 0) a)
(t(mygcd b (mod a b)))
))

(print (mygcd 10 100))
